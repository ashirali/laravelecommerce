<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Profile;

class UiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name'          => 'customer',
            'description'   => 'customer role'
        ]);
        $role = Role::create([
            'name'          => 'admin',
            'description'   => 'admin role'
        ]);
        $role = Role::create([
            'name'          => 'vendor',
            'description'   => 'vendor role'
        ]);
        $user = User::create([
        	'name'			=> 'admin',
            'email'         => 'admin@admin.com',
            'password'      => bcrypt('admin0101'),
            'roles_id'      => 2,
        ]);

        Profile::create([
            'user_id'       => $user -> id,
        ]);

    }
}
