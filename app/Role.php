<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{
    protected $guarded = [];
    public $table = 'roles';
    
   	public function users()
        {
        	
          return $this->hasMany('App\User');
        }
}
