<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	public $table = 'products';

    protected $guarded = [];

    public function order() {

    	return $this->hasMany('App\Order');
    }

}
