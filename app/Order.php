<?php

namespace App;

use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    public $table = 'orders';

    public function user() {

    	return $this->belongsTo('App\User');
    }

    public function product() {

    	return $this->belongsTo('App\Product');
    }
    
}
