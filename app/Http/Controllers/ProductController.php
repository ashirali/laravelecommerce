<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('pages.products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.addproduct');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $this->validate($request, [
            'name'                => 'required|min:1',
            'description'         => 'required|max:255',
            'price'               => 'required|max:255',
            'image'               => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);
            $image                = $request->file('image');
            $imagenewname         = rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images/'), $imagenewname);

            $product              = new Product();
            $product->name        = request('name');
            $product->description = request('description');
            $product->price       = request('price');
            $product->image       = $imagenewname;
            $product->save();
            $products = Product::all();

            return redirect()->route('addproduct')->withStatus(__('Product successfully created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, $id)
    {
        $products = Product::find($id);

        return view('pages.editproduct', compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product,$id)
    {
             $request->validate([
            'name'                => 'required|min:1',
            'description'         => 'required|min:1',
            'price'               => 'required|min:1',
            'image'               => 'required|image|mimes:jpg,png,jpeg|max:2048',
      ]);

            $image                = $request-> file('image');
            $imagenewname         = rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images/'), $imagenewname);

            $products             = Product::find($id);

            $product->name        = request('name');
            $product->description = request('description');
            $product->price       = request('price');
            $product->image       = $imagenewname;
            $product->save();

            return redirect()->route('productspreview')->withStatus(__('Product successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, $id)
    {
        $product = Product::where('id',$id)->delete();
        $products = Product::all();
        return redirect()->route('productspreview')->withStatus(__('Product successfully deleted.'));
    }
}
