@extends('ui.app', ['page' => __('Checkout'), 'pageSlug' => 'Checkout'])

@section('content')
    <div class="row ml-5 mr-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Checkout') }}</h5>
                </div>
                <form method="post" action="{{ route('checkout')}}" enctype="multipart/form-data">
                    <div class="card-body">
                            @csrf
                            @include('alerts.success')
                            <input type="hidden" name="product_id" value="{{ $products->id }}">
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Name') }}</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Name') }}" value="{{$products->name}}">
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Description') }}</label>
                                <input type="text" name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Description') }}" value="{{$products->description}}">
                                @include('alerts.feedback', ['field' => 'description'])
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Price') }}</label>
                                <input type="text" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Price') }}" value="{{$products->price}}">
                                @include('alerts.feedback', ['field' => 'price'])
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                            <label>Product Image</label>
                            <input type="file" name="image" class="homecartfile form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" value="{{$products->image}}">
                             @include('alerts.feedback', ['field' => 'image'])
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                            <label>Address</label>
                            <input type="text" name="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" value="">
                             @include('alerts.feedback', ['field' => 'address'])
                            </div>

                            <div class="form-group{{ $errors->has('qty') ? ' has-danger' : '' }}">
                            <label>Product Quantity</label>
                            <input type="text" name="qty" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }}" value="">
                             @include('alerts.feedback', ['field' => 'qty'])
                            </div>
                        
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-primary text-center">{{ __('Checkout') }}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
