@extends('layouts.app', ['page' => __('Add Product'), 'pageSlug' => 'Add Product'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Add Product') }}</h5>
                </div>
                <form method="post" action="{{ route('addproduct') }}" enctype="multipart/form-data">
                    <div class="card-body">
                            @csrf
                            @include('alerts.success')

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Name') }}</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Name') }}" value="">
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Description') }}</label>
                                <input type="text" name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Description') }}" value="">
                                @include('alerts.feedback', ['field' => 'description'])
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                <label>{{ __('Product Price') }}</label>
                                <input type="text" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="{{ __('Product Price') }}" value="">
                                @include('alerts.feedback', ['field' => 'price'])
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                            <label>Product Image</label>
                            <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                             @include('alerts.feedback', ['field' => 'image'])
                          </div>
                        
                        <div class="card-footer">
                            <button type="submit" class="btn btn-fill btn-primary">{{ __('Save Product') }}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
