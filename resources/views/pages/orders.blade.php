@extends('layouts.app', ['page' => __('Orders Table'), 'pageSlug' => 'Orders Table'])

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Order's Table</h4>
      </div>
        <p class="category pl-3">Here are the orders</p>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Order
                </th>
                <th>
                  Product
                </th>
                <th>
                  Address
                </th>
                <th>
                  Quantity
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr>
                <td>
                  {{$order->id}}
                </td>
                <td>
                  {{$order->product->name}}
                </td>
                <td>
                  {{$order->address}}
                </td>
                <td>
                  {{$order->qty}}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
