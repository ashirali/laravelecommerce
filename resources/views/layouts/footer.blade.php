<footer class="footer">
    <div class="container-fluid">
        <ul class="nav">
            <li class="nav-item">
                <a href="https://creative-tim.com" target="blank" class="nav-link">
                    {{ __('Welcome To Our Site') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{ __('About Us') }}
                </a>
            </li>
        </ul>
        <div class="copyright">
            <p>
            &copy; {{ now()->year }} <a href="">stopandbuy.com</a>
            </p>
        </div>
    </div>
</footer>
