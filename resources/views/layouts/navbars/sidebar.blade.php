<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text logo-mini">{{ __('AD') }}</a>
            <a href="" class="simple-text logo-normal">{{ __('Admin Dashboard') }}</a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug == 'dashboard') class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#laravel-examples" aria-expanded="true">
                    <i class="tim-icons icon-cart" ></i>
                    <span class="nav-link-text" >{{ __('Products') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse show" id="laravel-examples">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug == 'profile') class="active " @endif>
                            <a href="{{ route('productspreview')  }}">
                                <i class="tim-icons icon-settings"></i>
                                <p>{{ __('View Products') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li @if ($pageSlug == 'tables') class="active " @endif>
                <a href="{{ route('pages.tables') }}">
                    <i class="tim-icons icon-puzzle-10"></i>
                    <p>{{ __('Admin Role\'s Table') }}</p>
                </a>
            </li>
            <li @if ($pageSlug == 'registration') class="active " @endif>
                <a href="{{ route('registration') }}">
                    <i class="tim-icons icon-square-pin"></i>
                    <p>Registartion</p>
                </a>
            </li>
            <li @if ($pageSlug == 'orders') class="active " @endif>
                <a href="{{ route('orders') }}">
                    <i class="tim-icons icon-cart"></i>
                    <p>Orders</p>
                </a>
            </li>
        </ul>
    </div>
</div>
