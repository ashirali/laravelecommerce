@extends('ui.app')
@section('title', 'Home')
@section('slider')
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('images/img1.jpg')}}"  height="500px" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('images/img2.jpg')}}"  height="500px" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('images/img3.jpg')}}"  height="500px" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
@endsection
@section('productsheading')
	<h1 class="text-center mb-5">Products</h1>
  <div class="container-fluid">
    <div class="row">
      @foreach($products as $product)
      <div class="col-4">
        <div class="card mb-5 ml-5" style="width: 18rem;">
        <img class="card-img-top" src="{{asset('/images/'.$product->image)}}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">{{$product->name}}</h5>
          <p class="card-text">{{$product->description}}</p>
          <a href="{{ route('cart', $product->id)}}" class="btn btn-primary">Add to cart</a>
        </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
@endsection

