@extends('ui.app')
@section('title', 'Products')
@section('productsheading')
	<h1 class="text-center">Products</h1>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title" style="display:inline;"> Product's Table</h4>  
      </div>
        <p class=" pl-3" style="display:inline;">Here are the products</p>
        <div>
          <div class="pl-3">
            <a href="{{ route('addproduct') }}" type="submit" class="btn btn-success">{{ __('Add Product') }}</a>
          </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Description
                </th>
                <th>
                  Price
                </th>
                <th class="text-center">
                  Image
                </th>
                  <th class="text-center">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                <tr>
                  <td>
                    {{$product->name}}
                  </td>
                <td>
                   {{$product->description}}
                </td>
                <td>
                   {{$product->price}}
                </td>
                <td>
                <div class="">
                  <img src="{{asset('/images/'.$product->image)}}" height="150px" width="150px" class="" title="{{ $product->name }}">
                </div>
                </td>
                <td class="text-center">
                  <a href="products/{{$product->id}}/edit" class="btn btn-primary mr-1">Edit</a>
                  <a href="products/{{$product->id}}" class="btn btn-danger ml-1">Delete</a>
                </td>
              </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection