<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
		Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::get('/', function () {
     return view('welcome');
});

Route::get('/home', 'UiController@index')->name('index')->middleware('auth');

Route::get('/products', 'ProductController@index')->name('productspreview');

Route::get('/addproduct', 'ProductController@create');

Route::post('/addproduct', 'ProductController@store')->name('addproduct');

Route::get('/products/{id}', 'ProductController@destroy')->name('deleteproduct');

Route::get('/products/{id}/edit', 'ProductController@edit')->name('editproduct');

Route::post('/products/{id}/edit', 'ProductController@update')->name('updateproduct');

Route::get('/home/cart/{id}', 'OrderController@index')->name('cart');

Route::post('/home/checkout/', 'OrderController@store')->name('checkout');

Route::get('/orders', 'OrderController@show')->name('orders');

Route::get('/vendor', 'VendorRegistrationController@index')->name('registration');

Route::post('/vendor', 'VendorRegistrationController@store')->name('vendor');


